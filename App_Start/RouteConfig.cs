﻿using System.Web.Mvc;
using System.Web.Routing;

namespace SaveWedding
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //Home Routes
            routes.MapRoute(
                name: "Home",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
            //User routes
            routes.MapRoute(
                 name: "User",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                 name: "Information",
                 url: "{controller}/{action}/{id}",
                 defaults: new { controller = "Information", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "Planning",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Planning", action = "Index", id = UrlParameter.Optional }
           );
            routes.MapRoute(
                name: "Simulation",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Simulation", action = "Index", id = UrlParameter.Optional }
           );

            //Route Admin
            routes.MapRoute(
               name: "Admin",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
          );

        }
    }
}
