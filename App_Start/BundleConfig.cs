﻿using System.Web.Optimization;

namespace SaveWedding
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Parts of Home (Landing Page)
            bundles.Add(new StyleBundle("~/bundles/Homecss").Include(
                   "~/Templates/HomeTemp/assets/css/main.css",
              "~/Templates/HomeTemp/assets/css/noscript.css"));

            bundles.Add(new ScriptBundle("~/Content/Homejs").Include(
                      "~/Templates/HomeTemp/assets/js/jquery.min.js",
                      "~/Templates/HomeTemp/assets/js/jquery.scrolly.min.js",
                        "~/Templates/HomeTemp/assets/js/jquery.poptrox.min.js",
                        "~/Templates/HomeTemp/assets/js/browser.min.js",
                        "~/Templates/HomeTemp/assets/js/breakpoints.min.js",
                        "~/Templates/HomeTemp/assets/js/util.js",
                        "~/Templates/HomeTemp/assets/js/main.js"));

            // Parts of Sign In and Sign Up
            bundles.Add(new StyleBundle("~/bundles/Signincss").Include(
                "~/ Templates / PurpleTemp / assets / vendors / mdi / css / materialdesignicons.min.css",
                "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                "~/Templates/PurpleTemp/assets/css/style.css"));

            bundles.Add(new ScriptBundle("~/Content/Signinjs").Include(
                     "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                      "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                        "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                       "~/Templates/PurpleTemp/assets/js/misc.js"));


            bundles.Add(new StyleBundle("~/bundles/Signupcss").Include(
                "~/Templates/PurpleTemp/assets/vendors/mdi/css/materialdesignicons.min.css",
                "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                "~/Templates/PurpleTemp/assets/css/style.css"));

            bundles.Add(new ScriptBundle("~/Content/Signupjs").Include(
                    "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                    "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                    "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                    "~/Templates/PurpleTemp/assets/js/misc.js"));

            // Parts of User
            bundles.Add(new ScriptBundle("~/bundles/userjs").Include(
                      "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                      "~/Templates/PurpleTemp/assets/vendors/chart.js/Chart.min.js",
                      "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                      "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                      "~/Templates/PurpleTemp/assets/js/misc.js",
                      "~/Templates/PurpleTemp/assets/js/dashboard.js",
                      "~/Templates/PurpleTemp/assets/js/todolist.js"));

            bundles.Add(new StyleBundle("~/Content/usercss").Include(
                      "~/Templates/PurpleTemp/assets/vendors/mdi/css/materialdesignicons.min.css",
                      "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                      "~/Templates/PurpleTemp/assets/css/style.css",
                      "~/Templates/PurpleTemp/assets/images/favicon.png"));

            bundles.Add(new ScriptBundle("~/bundles/simulasijs").Include(
                    "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                    "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                    "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                    "~/Templates/PurpleTemp/assets/js/misc.js"));

            bundles.Add(new StyleBundle("~/Content/simulasicss").Include(
                    "~/Templates/PurpleTemp/assets/vendors/mdi/css/materialdesignicons.min.css",
                    "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                    "~/Templates/PurpleTemp/assets/css/style.css",
                    "~/Templates/PurpleTemp/assets/images/favicon.png"));

            bundles.Add(new ScriptBundle("~/bundles/planningjs").Include(
                  "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                  "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                  "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                  "~/Templates/PurpleTemp/assets/js/misc.js"));

            bundles.Add(new StyleBundle("~/Content/planningcss").Include(
                   "~/Templates/PurpleTemp/assets/vendors/mdi/css/materialdesignicons.min.css",
                   "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                   "~/Templates/PurpleTemp/assets/css/style.css",
                   "~/Templates/PurpleTemp/assets/images/favicon.png"));

            // Parts of Admin
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                     "~/Templates/PurpleTemp/assets/vendors/js/vendor.bundle.base.js",
                     "~/Templates/PurpleTemp/assets/vendors/chart.js/Chart.min.js",
                     "~/Templates/PurpleTemp/assets/js/off-canvas.js",
                     "~/Templates/PurpleTemp/assets/js/hoverable-collapse.js",
                     "~/Templates/PurpleTemp/assets/js/misc.js",
                     "~/Templates/PurpleTemp/assets/js/dashboard.js",
                     "~/Templates/PurpleTemp/assets/js/todolist.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Templates/PurpleTemp/assets/vendors/mdi/css/materialdesignicons.min.css",
                      "~/Templates/PurpleTemp/assets/vendors/css/vendor.bundle.base.css",
                      "~/Templates/PurpleTemp/assets/css/style.css",
                      "~/Templates/PurpleTemp/assets/images/favicon.png"));

            //Bundle for Datatables
            bundles.Add(new StyleBundle("~/bundles/datatable").Include(
                "~/Templates/PurpleTemp/assets/js/datatables/bootstrap.css",
                "~/Templates/PurpleTemp/assets/js/datatables/dataTables.bootstrap4.min.css"));

            bundles.Add(new ScriptBundle("~/Content/datatable").Include(
                "~/Templates/PurpleTemp/assets/js/datatables/jquery-3.3.1.js",
                "~/Templates/PurpleTemp/assets/js/datatables/jquery.dataTables.min.js",
                "~/Templates/PurpleTemp/assets/js/datatables/dataTables.bootstrap4.min.js"));

            //Bundle Carousel Home User
            bundles.Add(new ScriptBundle("~/Content/jquery").Include(
               "~/Templates/PurpleTemp/jquery.min.js"));
            bundles.Add(new ScriptBundle("~/Content/carousel").Include(
               "~/Templates/PurpleTemp/popper.min.js"));

        }
    }
}
