//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SaveWedding.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblStatusPlanning
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public tblStatusPlanning()
        {
            this.tblPlannings = new HashSet<tblPlanning>();
        }
    
        public int statusid { get; set; }
        public string statusname { get; set; }
        public string statusclass { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tblPlanning> tblPlannings { get; set; }
    }
}
