﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class PlanningStatus
    {
        public List<tblPlanning> plan { get; set; }
        public List<tblStatusPlanning> planstatus { get; set; }
    }
}