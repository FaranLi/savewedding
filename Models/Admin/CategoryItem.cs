﻿using System;
using System.Web;

namespace SaveWedding.Models.Admin
{
    public class CategoryItem
    {
        //table category content

        public string categoryname { get; set; }

        //table item content
        public long itemid { get; set; }
        public string itemname { get; set; }
        public string itemimage { get; set; }
        public string itemdetail { get; set; }
        public Nullable<long> itemprice { get; set; }

        public int categoryid { get; set; }

        public HttpPostedFileBase imagefile { get; set; }

    }
}