﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class JobSalaryCityUser
    {
        public List<tblSalary> Salary { get; set; }
        public List<tblJob> Job { get; set; }
        public List<tblCity> City { get; set; }
        public List<tblUser> User { get; set; }
        //public long userid { get; set; }
        //public string name { get; set; }
    }
}