﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class PackageItemCategory
    {
        public List<tblPackage> Package { get; set; }
        public List<tblCategory> Category { get; set; }
        public List<tblItem> Item { get; set; }
    }
}