﻿namespace SaveWedding.Models.Admin
{
    public class Dashboard
    {
        public int alregis { get; set; }
        public int cursave { get; set; }
        public int finsave { get; set; }
        public int latsave { get; set; }
        public int savday { get; set; }
        public int bonday { get; set; }
    }
}