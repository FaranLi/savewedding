﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class AccessActivation
    {
        public List<tblUser> UserList { get; set; }
        public List<tblActor> ActorList { get; set; }
    }
}