﻿using System;

namespace SaveWedding.Models.Admin
{
    public class UserActor
    {
        public long userid { get; set; }
        public string name { get; set; }
        public string username { get; set; }
        public string email { get; set; }
        public Nullable<int> isactive { get; set; }
        public Nullable<System.DateTime> datecreated { get; set; }
        public string phone { get; set; }
        public string noktp { get; set; }
        public string address { get; set; }
        public string imgprofile { get; set; }
        public Nullable<long> actorid { get; set; }
        public string Gender { get; set; }
        public string actorname { get; set; }


    }
}