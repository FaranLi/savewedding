﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class RewardUser
    {
        public List<UserActor> UserActor { get; set; }
        public List<tblReward> Reward { get; set; }
        public List<tblClaim> Claim { get; set; }
    }
}