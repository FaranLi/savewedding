﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class JobSalary
    {
        public List<tblJob> Joblist { get; set; }
        public List<tblSalary> Salarylist { get; set; }
    }
}