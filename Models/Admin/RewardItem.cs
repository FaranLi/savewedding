﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class RewardItem
    {
        public List<tblItem> Item { get; set; }
        public List<tblReward> Reward { get; set; }
    }
}