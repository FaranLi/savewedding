﻿using System.Collections.Generic;

namespace SaveWedding.Models.Admin
{
    public class SavingUser
    {
        public List<UserActor> UserActor { get; set; }
        public List<tblSaving> Saving { get; set; }
    }
}