﻿using SaveWedding.Models;
using System;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Security;


namespace SaveWedding.Controllers
{
    public class HomeController : Controller
    {
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SignIn()
        {
            if (Session["logedEmail"] != null)
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                return View();
            }

        }

        public static int countAttempt = 3;

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SignIn(tblUser item)
        {
            try
            {
                var lg = db.tblUsers.Where(a => a.email.Equals(item.email)).SingleOrDefault();
                if (lg != null)
                {
                    if (lg.isactive == 1)
                    {
                        if (lg.blockdate == null)
                        {
                            if (string.Compare(Crypto.Hash(item.password), lg.password) == 0)
                            {
                                if (lg.actorid == 2)
                                {
                                    Session["logedEmail"] = lg.email;
                                    Session["logedUserFullname"] = lg.name;
                                    Session["logedUsername"] = lg.username;
                                    Session["logedUserId"] = lg.userid;
                                    Session["logedActorId"] = lg.actorid;
                                    Session["logedUserImage"] = lg.imgprofile;
                                    TempData["iklansimulasi"] = "1";
                                    TempData.Keep();
                                    countAttempt = 3;
                                    return RedirectToAction("Index", "User");
                                }
                                else
                                {
                                    Session["logedEmail"] = lg.email;
                                    Session["logedUserFullname"] = lg.name;
                                    Session["logedUsername"] = lg.username;
                                    Session["logedUserId"] = lg.userid;
                                    Session["logedActorId"] = lg.actorid;
                                    Session["logedUserImage"] = lg.imgprofile;
                                    countAttempt = 3;
                                    return RedirectToAction("Index", "Admin");
                                }
                            }
                            else
                            {
                                countAttempt--;
                                if (countAttempt > 0)
                                {
                                    ViewBag.DuplicateMessage = "Invalid Password ! | Attemps Remaining: " + countAttempt;
                                    return View("SignIn");
                                }
                                else
                                {
                                    DateTime now = DateTime.Now;
                                    double H = now.Hour;
                                    double M = now.Minute;
                                    double S = now.Second;

                                    lg.blockdate = ((H * 3600) + (M * 60) + (S + 10));

                                    db.SaveChanges();
                                    ViewBag.DuplicateMessage = "Account being blocked in 10 seconds";
                                    return View("SignIn");
                                }
                            }
                        }
                        else
                        {
                            DateTime now = DateTime.Now;
                            double HH = now.Hour;
                            double MM = now.Minute;
                            double SS = now.Second;
                            double unblock = ((HH * 3600) + (MM * 60) + (SS));
                            if ((lg.blockdate - unblock) <= 0)
                            {
                                lg.blockdate = null;
                                db.SaveChanges();
                                countAttempt = 3;
                                return RedirectToAction("SignIn");
                            }
                            else
                            {
                                ViewBag.DuplicateMessage = "Account being blocked in " + (lg.blockdate - unblock) + " seconds";
                            }
                        }
                    }
                    else
                    {
                        ViewBag.DuplicateMessage = "Please activate the account first before sign in";
                    }
                }
                else
                {
                    ViewBag.DuplicateMessage = "Invalid email";

                }
                return View("SignIn");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }


        public JsonResult ForgotPassword(string emailaddress)
        {

            if (emailaddress != "")
            {
                var searchdata = db.tblUsers.Where(x => x.email == emailaddress).SingleOrDefault();
                string newpassword = Membership.GeneratePassword(6, 0);
                if (searchdata != null)
                {
                    searchdata.password = Crypto.Hash(newpassword);
                    db.SaveChanges();

                    string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "ResetPassword" + ".cshtml");
                    Body = Body.Replace("@ViewBag.Name", searchdata.name);
                    Body = Body.Replace("@ViewBag.NewPassword", newpassword);
                    Body = Body.ToString();

                    BuildEmailTemplate("Your password has been resetted!", Body, emailaddress);

                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                return Json(0, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(2, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SignUp()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SignUp(tblUser item)
        {
            var usernamedata = db.tblUsers.Where(x => x.username == item.username).SingleOrDefault();
            var emaildata = db.tblUsers.Where(x => x.email == item.email).SingleOrDefault();

            if (usernamedata == null && emaildata == null)
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    try
                    {
                        item.actorid = 2;
                        item.activationcode = Guid.NewGuid();
                        item.password = Crypto.Hash(item.password);
                        item.isactive = 0;
                        item.datecreated = DateTime.Now;
                        item.imgprofile = "~/Templates/Images/User/default.png";

                        db.tblUsers.Add(item);
                        db.SaveChanges();
                        TempData["success"] = "Your account has been registered, please Check confirmation link in your email for activating your account!";

                        BuildEmailTemplate(item.userid);
                        transaction.Commit();
                        return RedirectToAction("SignUp");
                    }
                    catch (Exception)
                    {
                        transaction.Rollback();
                        TempData["error"] = "Sending Email Activation is Failed!";
                        return View();
                    }
                }
            }
            else
            {
                if (usernamedata != null && emaildata == null)
                {
                    TempData["username"] = "Username is taken, Try Another!";
                }
                else if (emaildata != null && usernamedata == null)
                {
                    TempData["email"] = "Email is taken, Try Another!";
                }
                else
                {
                    TempData["email"] = "Email is taken, Try Another!";
                    TempData["username"] = "Username is taken, Try Another!";
                }
                return View();
            }

        }

        public ActionResult ConfirmSignUp(System.Guid ActivationCode, long userid)
        {
            ViewBag.ActivationCode = ActivationCode;
            ViewBag.userid = userid;
            return View();
        }

        public JsonResult SignUpConfirm(System.Guid ActivationCode, long userId)
        {
            tblUser data = db.tblUsers.Where(x => x.userid == userId).FirstOrDefault();
            string result = "";
            if (data.activationcode == ActivationCode)
            {
                data.isactive = 1;
                db.SaveChanges();
                result = "Your email is verified!";
            }
            else
            {
                result = "Your Activation Code is Wrong!";
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public void BuildEmailTemplate(long regid)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "Text" + ".cshtml");
            var reginfo = db.tblUsers.Where(x => x.userid == regid).FirstOrDefault();
            var dataurl = db.tblBaseUrls.Where(x => x.urlid == 1).SingleOrDefault();
            string url = dataurl.urllink + "Home/ConfirmSignUp?ActivationCode=" + reginfo.activationcode + "&userid=" + regid;

            Body = Body.Replace("@ViewBag.ConfirmationLink", url);
            Body = Body.ToString();
            BuildEmailTemplate("Your account is successfully created!", Body, reginfo.email);
        }

        public static void BuildEmailTemplate(string SubjectText, string BodyText, string SendTo)
        {
            string from, to, bcc, cc, subject, body;
            from = "saveweddingali@gmail.com";
            to = SendTo.Trim();
            bcc = "";
            cc = "";
            subject = SubjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(BodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from, "Save Wedding");
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }

        public static void SendEmail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("saveweddingali@gmail.com", "savewedding123");
            try
            {
                client.Send(mail);
            }
            catch (Exception error)
            {
                throw error;
            }
        }

        public ActionResult PermissionAccess()
        {
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View();
            }
        }

    }
}