﻿using SaveWedding.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class JobandSalaryController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: JobandSalary
        public ActionResult CreateJob(tblJob datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    try
                    {
                        db.tblJobs.Add(datalist);
                        db.SaveChanges();
                        TempData["message"] = "Success to add New Job Field";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("JobandSalary", "Admin");
            }
        }

        public ActionResult EditJob(tblJob datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    var datajob = db.tblJobs.Where(x => x.jobid == datalist.jobid).SingleOrDefault();
                    if (datajob != null)
                    {
                        try
                        {
                            datajob.jobname = datalist.jobname;
                            db.SaveChanges();
                            TempData["message"] = "Success to Edit Job Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["message"] = "Data Job is not Found";
                    }
                }
                return RedirectToAction("JobandSalary", "Admin");
            }
        }

        public ActionResult DeleteJob(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datajob = db.tblJobs.Find(id);
                if (datajob != null)
                {
                    try
                    {
                        db.tblJobs.Remove(datajob);
                        db.SaveChanges();

                        TempData["message"] = "Job Data has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }
                return RedirectToAction("JobandSalary", "Admin");
            }
        }

        public ActionResult CreateSalary(tblSalary datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    try
                    {
                        db.tblSalaries.Add(datalist);
                        db.SaveChanges();
                        TempData["message"] = "Success to add New Range Salary";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }

                return RedirectToAction("JobandSalary", "Admin");
            }
        }

        public ActionResult EditSalary(tblSalary datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    var datajob = db.tblSalaries.Where(x => x.salaryid == datalist.salaryid).SingleOrDefault();
                    if (datajob != null)
                    {
                        try
                        {
                            datajob.salaryrange = datalist.salaryrange;
                            db.SaveChanges();
                            TempData["message"] = "Success to Edit Salary Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["message"] = "Data Salary is not Found";
                    }
                }
                return RedirectToAction("JobandSalary", "Admin");
            }
        }

        public ActionResult DeleteSalary(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datasalary = db.tblSalaries.Find(id);
                if (datasalary != null)
                {
                    try
                    {
                        db.tblSalaries.Remove(datasalary);
                        db.SaveChanges();

                        TempData["message"] = "Range of Salary has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }
                return RedirectToAction("JobandSalary", "Admin");
            }
        }
    }
}