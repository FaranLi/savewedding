﻿using SaveWedding.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class DashboardController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Dashboard      
        public JsonResult AlreadyRegistered()
        {
            var datauser = db.tblUsers.Where(x => x.actorid == 2).Select(x => x.userid).ToArray();
            int data = 0;
            if (datauser != null)
            {
                data = datauser.Length;
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurrentlySaving()
        {
            var dataplanningid = db.tblPlannings.Where(x => x.statusid == 2).Select(x => x.userid).ToArray();
            long bufferid = 0;
            int data = 0;
            if (dataplanningid != null)
            {
                foreach (var item in dataplanningid)
                {
                    if (item != bufferid)
                    {
                        data += 1;
                        bufferid = Convert.ToInt64(item);
                    }
                }
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinishedSaving()
        {
            var dataplanningid = db.tblPlannings.Where(x => x.statusid == 1).Select(x => x.userid).ToArray();
            long bufferid = 0;
            int data = 0;
            if (dataplanningid != null)
            {
                foreach (var item in dataplanningid)
                {
                    if (item != bufferid)
                    {
                        data += 1;
                        bufferid = Convert.ToInt64(item);
                    }
                }
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LateSaving()
        {
            var datalatesave = db.tblSavings.Where(x => x.fine != null).Select(x => x.userid).ToArray();
            long bufferid3 = 0;
            int jumlatesave = 0;
            if (datalatesave != null)
            {
                foreach (var item3 in datalatesave)
                {
                    if (item3 != bufferid3)
                    {
                        jumlatesave += 1;
                        bufferid3 = Convert.ToInt64(item3);
                    }
                }
            }
            return Json(jumlatesave, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SavingToday()
        {
            var datasaveday = db.tblSavings.Where(x => x.uploaddate.Value.Day == DateTime.Now.Day && x.uploaddate.Value.Month == DateTime.Now.Month && x.uploaddate.Value.Year == DateTime.Now.Year).Count();
            int jumsavetoday = 0;
            if (datasaveday != 0)
            {
                jumsavetoday = datasaveday;
            }

            return Json(jumsavetoday, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetRewards()
        {
            var rewardata = db.tblClaims.OrderBy(x => x.userid).ToList();
            long distinctrewarddata = 0;
            int jumreward = 0;
            if (rewardata != null)
            {
                foreach (var itemsss in rewardata)
                {
                    if (itemsss.userid != distinctrewarddata)
                    {
                        jumreward += 1;
                        distinctrewarddata = itemsss.userid;
                    }
                    else
                    {
                        jumreward += 0;
                    }
                }
            }

            return Json(jumreward, JsonRequestBehavior.AllowGet);
        }
    }
}