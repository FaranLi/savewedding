﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class DecorationController : Controller
    {
        DBEntities db = new DBEntities();

        public ActionResult Create(CategoryItem datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                tblItem itemdata = new tblItem();
                try
                {
                    string fileName = Path.GetFileNameWithoutExtension(datalist.imagefile.FileName);
                    string extension = Path.GetExtension(datalist.imagefile.FileName);
                    fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                    itemdata.categoryid = 2;
                    itemdata.itemname = datalist.itemname;
                    itemdata.itemdetail = datalist.itemdetail;
                    itemdata.itemprice = datalist.itemprice;
                    itemdata.itemimage = "~/Templates/Images/Category/" + fileName;
                    fileName = Path.Combine(Server.MapPath("~/Templates/Images/Category/"), fileName);
                    datalist.imagefile.SaveAs(fileName);

                    db.tblItems.Add(itemdata);
                    db.SaveChanges();
                    TempData["message"] = "Success to add a Decoration Item.";
                }
                catch (Exception error)
                {
                    throw error;
                }
                ModelState.Clear();
                return RedirectToAction("Decoration", "Admin");
            }
        }

        public ActionResult Edit(CategoryItem datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                tblItem itemdata = new tblItem();
                var dataitem = db.tblItems.Where(x => x.itemid == datalist.itemid).SingleOrDefault();
                if (dataitem != null)
                {
                    try
                    {
                        //check image input empty or not
                        if (datalist.imagefile != null)
                        {
                            //delete old image before update
                            string fullPath = Request.MapPath(dataitem.itemimage);
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }
                            string fileName = Path.GetFileNameWithoutExtension(datalist.imagefile.FileName);
                            string extension = Path.GetExtension(datalist.imagefile.FileName);
                            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                            dataitem.itemimage = "~/Templates/Images/Category/" + fileName;
                            fileName = Path.Combine(Server.MapPath("~/Templates/Images/Category/"), fileName);
                            datalist.imagefile.SaveAs(fileName);
                        }
                        //moving data from category item to tbl item
                        dataitem.itemname = datalist.itemname;
                        dataitem.itemprice = datalist.itemprice;
                        dataitem.itemdetail = datalist.itemdetail;

                        db.SaveChanges();
                        TempData["message"] = "Success to Edit Item Data";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is Unavailable";
                }
                ModelState.Clear();
                return RedirectToAction("Decoration", "Admin");
            }
        }

        public ActionResult Delete(long id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                try
                {
                    var dataitem = db.tblItems.Find(id);
                    var fileimage = db.tblItems.Where(x => x.itemid == id).SingleOrDefault();
                    if (dataitem != null)
                    {
                        //delete image in folder path                    
                        string fullPath = Request.MapPath(fileimage.itemimage);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                        //delete data in database
                        db.tblItems.Remove(dataitem);
                        db.SaveChanges();
                        TempData["message"] = "Item Data is Successed to be Deleted";
                    }
                    else
                    {
                        TempData["Message"] = "Item is not Found!";
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
                return RedirectToAction("Decoration", "Admin");
            }
        }
    }
}