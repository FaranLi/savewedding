﻿using SaveWedding.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class CityController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: City
        public ActionResult CreateCity(tblCity datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    try
                    {
                        db.tblCities.Add(datalist);
                        db.SaveChanges();
                        TempData["message"] = "Success to add New City";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("DistrictorCity", "Admin");
            }
        }

        public ActionResult EditCity(tblCity datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    var datacity = db.tblCities.Where(x => x.cityid == datalist.cityid).SingleOrDefault();
                    if (datacity != null)
                    {
                        try
                        {
                            datacity.cityname = datalist.cityname;
                            db.SaveChanges();
                            TempData["message"] = "Success to Edit City Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["message"] = "Data City is not Found";
                    }
                }
                return RedirectToAction("DistrictorCity", "Admin");
            }
        }

        public ActionResult DeleteCity(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datacity = db.tblCities.Find(id);
                if (datacity != null)
                {
                    try
                    {
                        db.tblCities.Remove(datacity);
                        db.SaveChanges();

                        TempData["message"] = "City has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }
                return RedirectToAction("DistrictorCity", "Admin");
            }
        }
    }
}