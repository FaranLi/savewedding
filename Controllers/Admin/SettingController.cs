﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class SettingController : Controller
    {
        // GET: Setting
        DBEntities db = new DBEntities();
        public ActionResult PlanningStatus()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datalist = db.tblStatusPlannings.ToList();

                return View(datalist);
            }
        }

        public ActionResult CreateStatusPlanning(tblStatusPlanning datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    try
                    {
                        db.tblStatusPlannings.Add(datalist);
                        db.SaveChanges();
                        TempData["message"] = "Success to add New Status Planning";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("PlanningStatus", "Setting");
            }
        }

        public ActionResult EditStatusPlanning(tblStatusPlanning datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    var datastatus = db.tblStatusPlannings.Where(x => x.statusid == datalist.statusid).SingleOrDefault();
                    if (datastatus != null)
                    {
                        try
                        {
                            datastatus.statusname = datalist.statusname;
                            datastatus.statusclass = datalist.statusclass;
                            db.SaveChanges();
                            TempData["message"] = "Success to Edit Status Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["message"] = "Data Status is not Found";
                    }
                }
                return RedirectToAction("PlanningStatus", "Setting");
            }
        }

        public ActionResult DeleteStatusPlanning(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datastatus = db.tblStatusPlannings.Find(id);
                if (datastatus != null)
                {
                    try
                    {
                        db.tblStatusPlannings.Remove(datastatus);
                        db.SaveChanges();

                        TempData["message"] = "Status has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }
                return RedirectToAction("PlanningStatus", "Setting");
            }
        }

        public ActionResult AccessActiveUser()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var listuser = db.tblUsers.Where(x => x.userid != 1).ToList();
                var listactor = db.tblActors.ToList();

                AccessActivation datalist = new AccessActivation();
                datalist.UserList = listuser;
                datalist.ActorList = listactor;

                return View(datalist);
            }
        }

        public ActionResult EditAccessActivation(long userid, long actorid, int isactive)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var dataaccess = db.tblUsers.Where(x => x.userid == userid).SingleOrDefault();
                if (dataaccess != null)
                {
                    try
                    {
                        dataaccess.actorid = actorid;
                        dataaccess.isactive = isactive;
                        db.SaveChanges();
                        TempData["message"] = "Success to Edit User Data";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data User is not Found";
                }

                return RedirectToAction("AccessActiveUser", "Setting");
            }
        }
        public ActionResult EditActivation(long userid, int isactive)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var dataaccess = db.tblUsers.Where(x => x.userid == userid).SingleOrDefault();
                if (dataaccess != null)
                {
                    try
                    {
                        dataaccess.isactive = isactive;
                        db.SaveChanges();
                        TempData["message"] = "Success to Edit User Data";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data User is not Found";
                }

                return RedirectToAction("AccessActiveUser", "Setting");
            }
        }

        public ActionResult DeleteCustomer(long id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datauser = db.tblUsers.Find(id);
                if (datauser != null)
                {
                    try
                    {
                        db.tblUsers.Remove(datauser);
                        db.SaveChanges();

                        TempData["message"] = "The User has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }

                return RedirectToAction("AccessActiveUser", "Setting");
            }
        }
    }
}