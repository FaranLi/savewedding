﻿using SaveWedding.Models;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class RewardController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Reward
        public ActionResult CreateReward(tblReward itemdata, HttpPostedFileBase imagefile)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datareward = db.tblRewards.Select(x => x.rewardid).ToArray();

                if (itemdata != null)
                {
                    try
                    {
                        string fileName = Path.GetFileNameWithoutExtension(imagefile.FileName);
                        string extension = Path.GetExtension(imagefile.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                        itemdata.imagebanner = "~/Templates/Images/RewardBanner/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Templates/Images/RewardBanner/"), fileName);
                        imagefile.SaveAs(fileName);
                        itemdata.createdate = DateTime.Now;
                        itemdata.status = 0;
                        db.tblRewards.Add(itemdata);
                        db.SaveChanges();
                        TempData["message"] = "Success to add New Reward";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }

                return RedirectToAction("Reward", "Admin");
            }
        }
        public ActionResult Edit(tblReward itemdata, HttpPostedFileBase imagefile)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var dataitem = db.tblRewards.Where(x => x.rewardid == itemdata.rewardid).SingleOrDefault();
                if (dataitem != null)
                {
                    if (dataitem.status != 1 || dataitem.status == null)
                    {
                        try
                        {
                            //check image input empty or not
                            if (imagefile != null)
                            {
                                //delete old image before update
                                string fullPath = Request.MapPath(dataitem.imagebanner);
                                if (System.IO.File.Exists(fullPath))
                                {
                                    System.IO.File.Delete(fullPath);
                                }
                                string fileName = Path.GetFileNameWithoutExtension(imagefile.FileName);
                                string extension = Path.GetExtension(imagefile.FileName);
                                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                                dataitem.imagebanner = "~/Templates/Images/RewardBanner/" + fileName;
                                fileName = Path.Combine(Server.MapPath("~/Templates/Images/RewardBanner/"), fileName);
                                imagefile.SaveAs(fileName);
                            }
                            //moving data from category item to tbl item
                            dataitem.rewardname = itemdata.rewardname;
                            dataitem.totalpoint = itemdata.totalpoint;
                            dataitem.createdate = DateTime.Now;

                            db.SaveChanges();
                            TempData["message"] = "Success to Edit Reward Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["warning"] = "Please change status to not active before edit";
                    }

                }
                else
                {
                    TempData["warning"] = "Data is Unavailable";
                }
                ModelState.Clear();
                return RedirectToAction("Reward", "Admin");
            }
        }

        public ActionResult Delete(long id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                try
                {
                    var dataitem = db.tblRewards.Find(id);
                    var fileimage = db.tblRewards.Where(x => x.rewardid == id).SingleOrDefault();
                    if (dataitem != null)
                    {
                        //delete image in folder path                    
                        string fullPath = Request.MapPath(fileimage.imagebanner);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                        //delete data in database
                        db.tblRewards.Remove(dataitem);
                        db.SaveChanges();
                        TempData["message"] = "Reward Data is Successed to be Deleted";
                    }
                    else
                    {
                        TempData["warning"] = "Reward is not Found!";
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
                return RedirectToAction("Reward", "Admin");
            }
        }

        public ActionResult EditStatusReward(long Idreward, int status)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var rewarddata = db.tblRewards.Where(x => x.rewardid == Idreward).SingleOrDefault();

                if (rewarddata != null)
                {
                    try
                    {
                        if (status == 1 && rewarddata.status != 1)
                        {
                            rewarddata.status = 1;
                            db.SaveChanges();


                            TempData["message"] = rewarddata.rewardname + " Actived";
                        }
                        else if (status == 0 && rewarddata.status != 0)
                        {
                            rewarddata.status = 0;
                            db.SaveChanges();
                            TempData["message"] = rewarddata.rewardname + " Not Actived";
                        }
                        else
                        {
                            TempData["warning"] = "Failed to change, status was same before.";
                        }

                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }

                return RedirectToAction("Reward", "Admin");
            }
        }
    }
}