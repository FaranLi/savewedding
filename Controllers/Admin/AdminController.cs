﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class AdminController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Admin
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                Dashboard dashboardata = new Dashboard();
                //get already registered data
                var datauser = db.tblUsers.Where(x => x.actorid == 2).Select(x => x.userid).ToArray();
                int jumregis = 0;
                if (datauser != null)
                {
                    jumregis = datauser.Length;
                    dashboardata.alregis = jumregis;
                }

                //get currently saving data
                var dataplanningid = db.tblPlannings.Where(x => x.statusid == 2).Select(x => x.userid).ToArray();
                long bufferid = 0;
                int jumcurrentsave = 0;
                if (dataplanningid != null)
                {
                    foreach (var item in dataplanningid)
                    {
                        if (item != bufferid)
                        {
                            jumcurrentsave += 1;
                            bufferid = Convert.ToInt64(item);
                        }
                    }
                    dashboardata.cursave = jumcurrentsave;
                }

                //get Finished saving data
                var dataplanningid2 = db.tblPlannings.Where(x => x.statusid == 1).Select(x => x.userid).ToArray();
                long bufferid2 = 0;
                int jumfinishsave = 0;
                if (dataplanningid2 != null)
                {
                    foreach (var item2 in dataplanningid2)
                    {
                        if (item2 != bufferid2)
                        {
                            jumfinishsave += 1;
                            bufferid2 = Convert.ToInt64(item2);
                        }
                    }
                    dashboardata.finsave = jumfinishsave;
                }

                //get Late Saving data
                var datalatesave = db.tblSavings.Where(x => x.fine != null).Select(x => x.userid).ToArray();
                long bufferid3 = 0;
                int jumlatesave = 0;
                if (datalatesave != null)
                {
                    foreach (var item3 in datalatesave)
                    {
                        if (item3 != bufferid3)
                        {
                            jumlatesave += 1;
                            bufferid3 = Convert.ToInt64(item3);
                        }
                    }
                    dashboardata.latsave = jumlatesave;
                }

                //get Saving Today data
                var datasaveday = db.tblSavings.Where(x => x.uploaddate.Value.Day == DateTime.Now.Day && x.uploaddate.Value.Month == DateTime.Now.Month && x.uploaddate.Value.Year == DateTime.Now.Year).Count();
                int jumsavetoday = 0;
                if (datasaveday != 0)
                {
                    jumsavetoday = datasaveday;
                }
                dashboardata.savday = jumsavetoday;

                //get reward data customer
                var rewardata = db.tblClaims.OrderBy(x => x.userid).ToList();
                long distinctrewarddata = 0;
                int jumreward = 0;
                if (rewardata != null)
                {
                    foreach (var itemsss in rewardata)
                    {
                        if (itemsss.userid != distinctrewarddata)
                        {
                            jumreward += 1;
                            distinctrewarddata = itemsss.userid;
                        }
                        else
                        {
                            jumreward += 0;
                        }
                    }
                    dashboardata.bonday = jumreward;
                }

                return View(dashboardata);
            }
        }

        public ActionResult Customer()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblUser> Customerlist = db.tblUsers.Where(x => x.actorid == 2).ToList();
                List<tblSaving> Savinglist = db.tblSavings.ToList();

                UserActor VCustomer = new UserActor();
                List<UserActor> listcustomer = Customerlist.Select(x => new UserActor
                {
                    userid = x.userid,
                    name = x.name,
                    username = x.username,
                    email = x.email,
                    isactive = x.isactive,
                    datecreated = x.datecreated,
                    phone = x.phone,
                    noktp = x.noktp,
                    address = x.address,
                    imgprofile = x.imgprofile,
                    actorid = x.actorid,
                    Gender = x.Gender,
                    actorname = x.tblActor.actor,

                }).ToList();

                SavingUser datalist = new SavingUser();
                datalist.UserActor = listcustomer;
                datalist.Saving = Savinglist;


                return View(datalist);
            }
        }
        public ActionResult Saving()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblUser> Customerlist = db.tblUsers.Where(x => x.actorid == 2).ToList();
                List<tblSaving> Savinglist = db.tblSavings.ToList();

                UserActor VCustomer = new UserActor();
                List<UserActor> listcustomer = Customerlist.Select(x => new UserActor
                {
                    userid = x.userid,
                    name = x.name,

                }).ToList();

                SavingUser datalist = new SavingUser();
                datalist.UserActor = listcustomer;
                datalist.Saving = Savinglist;
                return View(datalist);
            }
        }
        public ActionResult Claim()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblUser> Customerlist = db.tblUsers.Where(x => x.actorid == 2).ToList();
                List<tblClaim> claimlist = db.tblClaims.ToList();
                List<tblReward> rewardlist = db.tblRewards.ToList();

                UserActor VCustomer = new UserActor();
                List<UserActor> listcustomer = Customerlist.Select(x => new UserActor
                {
                    userid = x.userid,
                    name = x.name,
                    email = x.email

                }).ToList();

                RewardUser datalist = new RewardUser();
                datalist.UserActor = listcustomer;
                datalist.Claim = claimlist;
                datalist.Reward = rewardlist;
                return View(datalist);
            }
        }

        public ActionResult Planning()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var dataplanning = db.tblPlannings.ToList();
                var status = db.tblStatusPlannings.ToList();

                PlanningStatus listplanning = new PlanningStatus();
                listplanning.plan = dataplanning;
                listplanning.planstatus = status;

                return View(listplanning);
            }
        }

        public ActionResult DeleteCustomer(long id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datauser = db.tblUsers.Find(id);
                if (datauser != null)
                {
                    try
                    {
                        db.tblUsers.Remove(datauser);
                        db.SaveChanges();

                        TempData["message"] = "The Customer has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["message"] = "Data is not Found!";
                }

                return RedirectToAction("Customer");
            }
        }

        public ActionResult JobandSalary()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblJob> listjob = db.tblJobs.ToList();
                List<tblSalary> listsalary = db.tblSalaries.ToList();

                JobSalary datalist = new JobSalary();
                datalist.Joblist = listjob;
                datalist.Salarylist = listsalary;

                return View(datalist);
            }
        }

        public ActionResult DistrictorCity()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblCity> citylist = db.tblCities.ToList();

                return View(citylist);
            }
        }
        public ActionResult Reward()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblReward> rewardlist = db.tblRewards.ToList();

                return View(rewardlist);
            }
        }

        public ActionResult Package()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblPackage> packagelist = db.tblPackages.ToList();
                List<tblCategory> categorylist = db.tblCategories.ToList();
                List<tblItem> itemlist = db.tblItems.ToList();

                PackageItemCategory datalist = new PackageItemCategory();
                datalist.Package = packagelist;
                datalist.Category = categorylist;
                datalist.Item = itemlist;

                return View(datalist);
            }
        }

        public ActionResult Dress()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblItem> itemlist = db.tblItems.Where(x => x.categoryid == 1 && x.packageid == null).ToList();

                List<CategoryItem> datalist = itemlist.Select(x => new CategoryItem
                {
                    itemid = x.itemid,
                    itemname = x.itemname,
                    itemimage = x.itemimage,
                    itemdetail = x.itemdetail,
                    itemprice = x.itemprice,
                    categoryid = x.tblCategory.categoryid,
                    categoryname = x.tblCategory.categoryname
                }).ToList();
                return View(datalist);
            }
        }

        public ActionResult Decoration()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblItem> itemlist = db.tblItems.Where(x => x.categoryid == 2 && x.packageid == null).ToList();

                List<CategoryItem> datalist = itemlist.Select(x => new CategoryItem
                {
                    itemid = x.itemid,
                    itemname = x.itemname,
                    itemimage = x.itemimage,
                    itemdetail = x.itemdetail,
                    itemprice = x.itemprice,
                    categoryid = x.tblCategory.categoryid,
                    categoryname = x.tblCategory.categoryname
                }).ToList();
                return View(datalist);
            }
        }

        public ActionResult Invitation()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblItem> itemlist = db.tblItems.Where(x => x.categoryid == 3 && x.packageid == null).ToList();

                List<CategoryItem> datalist = itemlist.Select(x => new CategoryItem
                {
                    itemid = x.itemid,
                    itemname = x.itemname,
                    itemimage = x.itemimage,
                    itemdetail = x.itemdetail,
                    itemprice = x.itemprice,
                    categoryid = x.tblCategory.categoryid,
                    categoryname = x.tblCategory.categoryname
                }).ToList();
                return View(datalist);
            }
        }

        public ActionResult Catering()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblItem> itemlist = db.tblItems.Where(x => x.categoryid == 4 && x.packageid == null).ToList();

                List<CategoryItem> datalist = itemlist.Select(x => new CategoryItem
                {
                    itemid = x.itemid,
                    itemname = x.itemname,
                    itemimage = x.itemimage,
                    itemdetail = x.itemdetail,
                    itemprice = x.itemprice,
                    categoryid = x.tblCategory.categoryid,
                    categoryname = x.tblCategory.categoryname
                }).ToList();
                return View(datalist);
            }
        }

        public ActionResult LogOut()
        {
            Session.Clear();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }



        public ActionResult ChangePassword()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(string Kode, tblUser item)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                try
                {
                    var emailuser = Session["logedEmail"].ToString();
                    var cp = db.tblUsers.Where(a => a.email.Equals(emailuser)).SingleOrDefault();
                    if (cp != null)
                    {

                        if (string.Compare(Crypto.Hash(item.password), cp.password) == 0)
                        {

                            cp.password = Crypto.Hash(Kode);
                            db.SaveChanges();
                            TempData["success"] = "Your password has been changed!";
                            return RedirectToAction("ChangePassword");
                        }
                        else
                        {
                            TempData["error"] = "Your old password is wrong !";
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.DuplicateMessage = "Invalid email";

                    }
                    return View("ChangePassword");
                }
                catch (Exception error)
                {
                    TempData["error"] = error.Message;
                    return View();
                }
            }
        }

        public ActionResult ViewProfile()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                int id = Convert.ToInt32(Session["logedUserId"]);
                var userprofile = db.tblUsers.Where(x => x.userid == id).FirstOrDefault();
                return View(userprofile);
            }
        }

        [HttpPost]
        public ActionResult ViewProfile(tblUser user, HttpPostedFileBase image1)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var userdata = db.tblUsers.Where(x => x.userid == user.userid).SingleOrDefault();
                //Update fields
                if (userdata != null)
                {
                    try
                    {
                        if (image1 != null)
                        {
                            //delete old image before update
                            string fullPath = Request.MapPath(userdata.imgprofile);
                            if (System.IO.File.Exists(fullPath) && userdata.imgprofile != "~/Templates/Images/User/default.png")
                            {
                                System.IO.File.Delete(fullPath);
                            }
                            string fileName = Path.GetFileNameWithoutExtension(image1.FileName);
                            string extension = Path.GetExtension(image1.FileName);
                            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                            userdata.imgprofile = "~/Templates/Images/User/" + fileName;
                            fileName = Path.Combine(Server.MapPath("~/Templates/Images/User/"), fileName);
                            image1.SaveAs(fileName);
                        }
                        userdata.name = user.name;
                        userdata.username = user.username;
                        userdata.phone = user.phone;
                        userdata.noktp = user.noktp;
                        userdata.Gender = user.Gender;
                        userdata.address = user.address;


                        db.SaveChanges();
                        Session["logedUserImage"] = userdata.imgprofile;
                        Session["logedUsername"] = userdata.username;
                        return RedirectToAction("ViewProfile", "Admin");
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("ViewProfile", "Admin");
            }
        }
        public JsonResult AjaxPostCall(Category idData)
        {
            db.Configuration.ProxyCreationEnabled = false;
            int selectedCat = Convert.ToInt32(idData.Id);
            var dataItem = db.tblItems.Where(x => x.categoryid == selectedCat && x.packageid == null).ToList();

            return Json(dataItem, JsonRequestBehavior.AllowGet);
        }

        public class Category
        {
            public string Id
            {
                get;
                set;
            }

        }
    }
}