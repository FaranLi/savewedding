﻿using SaveWedding.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.Admin
{
    public class PackageController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Package
        public ActionResult CreatePackage(tblPackage datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datapackage = db.tblPackages.Select(x => x.packageid).ToArray();
                int jumpackage = datapackage.Length;
                if (jumpackage >= 3)
                {
                    TempData["warning"] = "Maximum Package can be Created only 3 Packages!";
                }
                else
                {
                    if (datalist != null)
                    {
                        try
                        {
                            db.tblPackages.Add(datalist);
                            db.SaveChanges();
                            TempData["hasil"] = "Success to add New Package";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                }
                return RedirectToAction("Package", "Admin");
            }
        }

        public ActionResult ItemPackage(tblItem itemlist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (itemlist != null)
                {
                    try
                    {
                        var itempackage = db.tblItems.Where(x => x.itemid == itemlist.itemid).Select(x => new { x.itemname, x.categoryid, x.itemdetail, x.itemprice, x.itemimage }).SingleOrDefault();
                        tblItem item = new tblItem();
                        item.itemname = itempackage.itemname;
                        item.itemdetail = itempackage.itemdetail;
                        item.itemimage = itempackage.itemimage;
                        item.categoryid = itempackage.categoryid;
                        item.itemprice = itempackage.itemprice;
                        item.packageid = itemlist.packageid;
                        db.tblItems.Add(item);
                        db.SaveChanges();
                        TempData["hasil"] = "Success to add New Item Package";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }

                return RedirectToAction("Package", "Admin");
            }
        }

        public ActionResult EditPackage(tblPackage datalist)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (datalist != null)
                {
                    var datapackage = db.tblPackages.Where(x => x.packageid == datalist.packageid).SingleOrDefault();
                    if (datapackage != null)
                    {
                        try
                        {
                            datapackage.packagename = datalist.packagename;
                            db.SaveChanges();
                            TempData["hasil"] = "Success to Edit Package Data";
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }
                    else
                    {
                        TempData["hasil"] = "Data Package is not Found";
                    }
                }
                return RedirectToAction("Package", "Admin");
            }
        }

        public ActionResult DeletePackage(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datapackage = db.tblPackages.Find(id);
                if (datapackage != null)
                {
                    try
                    {
                        db.tblPackages.Remove(datapackage);
                        db.SaveChanges();

                        TempData["hasil"] = "Package has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["hasil"] = "Data is not Found!";
                }
                return RedirectToAction("Package", "Admin");
            }
        }

        public ActionResult DeleteItemPackage(int id)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var datapackage = db.tblItems.Find(id);
                if (datapackage != null)
                {
                    try
                    {
                        db.tblItems.Remove(datapackage);
                        db.SaveChanges();

                        TempData["hasil"] = "Item Package has been Deleted!";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                else
                {
                    TempData["hasil"] = "Data is not Found!";
                }
                return RedirectToAction("Package", "Admin");
            }
        }

    }
}