﻿using SaveWedding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class ClaimController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Claim
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                long iduser = Convert.ToInt64(Session["logedUserId"]);
                List<tblClaim> claimdata = db.tblClaims.Where(x => x.userid == iduser).ToList();

                return View(claimdata);
            }
        }

        public ActionResult EditStatusClaim(long Idclaim, int status)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var claimddata = db.tblClaims.Where(x => x.claimid == Idclaim).SingleOrDefault();

                if (claimddata != null)
                {
                    try
                    {
                        claimddata.claimstatus = 1;
                        claimddata.usedate = DateTime.Now;
                        db.SaveChanges();

                        TempData["message"] = claimddata.tblReward.rewardname + " Used";
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                    long claimId = claimddata.claimid;
                    BuildEmailTemplate(claimId);
                }

                return RedirectToAction("Index", "Claim");
            }
        }
        public ActionResult ClaimReward(string rewardpoint, string rewardid)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                if (rewardpoint != "")
                {
                    long iduser = Convert.ToInt64(Session["logedUserId"]);

                    var savingdata = db.tblSavings.Where(x => x.userid == iduser).OrderByDescending(x => x.savingid).FirstOrDefault();
                    if (savingdata != null)
                    {
                        var totalpoint = Math.Round(Convert.ToDouble(savingdata.point), 0);
                        tblClaim claim = new tblClaim();
                        int Point = Convert.ToInt32(rewardpoint);
                        long Idreward = Convert.ToInt64(rewardid);
                        if (totalpoint >= Point)
                        {
                            double kurang = totalpoint - Point;
                            claim.userid = iduser;
                            claim.rewardid = Idreward;
                            claim.claimstatus = 0;
                            claim.claimdate = DateTime.Now;
                            db.tblClaims.Add(claim);
                            db.SaveChanges();

                            savingdata.point = kurang;
                            db.SaveChanges();

                            TempData["claimresult"] = "1";
                        }
                        else
                        {
                            TempData["claimresult"] = "2";
                        }
                    }
                    else
                    {
                        TempData["claimresult"] = "2";
                    }
                }
                TempData.Keep();
                return RedirectToAction("Index", "User");
            }
        }

        public void BuildEmailTemplate(long Idclaim)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "UseReward" + ".cshtml");
            var rewardinfo = db.tblClaims.Where(x => x.claimid == Idclaim).FirstOrDefault();

            var datauser = db.tblUsers.Where(x => x.userid == rewardinfo.userid).FirstOrDefault();

            var pointo = rewardinfo.tblReward.totalpoint.Value.ToString("N0");
            Body = Body.Replace("@ViewBag.rewardname", rewardinfo.tblReward.rewardname);
            Body = Body.Replace("@ViewBag.Name", datauser.name);
            Body = Body.Replace("@ViewBag.point", pointo);
            Body = Body.Replace("@ViewBag.usedate", rewardinfo.usedate.Value.ToString("dd/MM/yyyy HH:mm"));
            Body = Body.ToString();
            BuildEmailTemplate("Your Reward Use Confirmation from Save Wedding!", Body, datauser.email);

        }

        public static void BuildEmailTemplate(string SubjectText, string BodyText, string SendTo)
        {
            string from, to, bcc, cc, subject, body;
            from = "saveweddingali@gmail.com";
            to = SendTo.Trim();
            bcc = "";
            cc = "";
            subject = SubjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(BodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from, "Save Wedding");
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }

        public static void SendEmail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("saveweddingali@gmail.com", "savewedding123");
            try
            {
                client.Send(mail);
            }
            catch (Exception error)
            {
                throw error;
            }
        }

    }
}