﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class PlanningController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Planning
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblPackage> packagedata = db.tblPackages.ToList();
                List<tblItem> itemdata = db.tblItems.Where(x => x.packageid == null).ToList();
                List<tblCategory> categorydata = db.tblCategories.ToList();

                PackageItemCategory datalist = new PackageItemCategory();
                datalist.Package = packagedata;
                datalist.Item = itemdata;
                datalist.Category = categorydata;

                return View(datalist);
            }
        }

        [HttpPost]
        public ActionResult Index(long[] itemid)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                //get data from form index planning
                string packageid = Request.Params["packageid"];
                string months = Request.Params["months"];
                string years = Request.Params["years"];
                string totalprice = Request.Params["totalprice"];
                string monthlyfee = Request.Params["monthlyfee"];
                long iduser = Convert.ToInt64(Session["logedUserId"]);


                var historyplanning = db.tblPlannings.Where(x => x.userid == iduser && x.statusid == 2).FirstOrDefault();
                if (historyplanning != null)
                {
                    TempData["message"] = "You have been Joining Planning Wedding, Please Finish your Current Planning Wedding before Joining Planning Wedding Again!";
                }
                else if (packageid == "0" && itemid == null)
                {
                    TempData["message"] = "Please Choose Package or Custom Package!";
                }
                else if (totalprice == "" || CheckPriceItem(itemid, packageid) != totalprice)
                {
                    TempData["message"] = "Please Click Button Calculate in every changing of Custom or Package Wedding that You have Done!";
                }
                else
                {
                    tblPlanning planuser = new tblPlanning();
                    int totalmonth;
                    if (months != "")
                    {
                        totalmonth = (Convert.ToInt32(years) * 12) + Convert.ToInt32(months);
                    }
                    else
                    {
                        totalmonth = (Convert.ToInt32(years) * 12);
                    }

                    long price = Convert.ToInt64(totalprice);
                    long fee = Convert.ToInt64(monthlyfee);

                    //input data custom package first
                    if (itemid != null)
                    {
                        try
                        {
                            foreach (long item in itemid)
                            {
                                planuser.plandate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                                planuser.userid = iduser;
                                planuser.longtimesaving = totalmonth;
                                planuser.totalprice = price;
                                planuser.monthlyfee = fee;
                                planuser.itemid = item;
                                planuser.statusid = 2;
                                db.tblPlannings.Add(planuser);
                                db.SaveChanges();
                            }
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                    }

                    int idpackag = Convert.ToInt32(packageid);
                    long[] iditem = itemid;
                    itemid = db.tblItems.Where(x => x.packageid == idpackag).Select(x => x.itemid).ToArray();
                    if (itemid != null)
                    {
                        try
                        {
                            foreach (long item in itemid)
                            {
                                planuser.plandate = DateTime.Now.ToString("dd/MM/yyyy HH:mm");
                                planuser.userid = iduser;
                                planuser.longtimesaving = totalmonth;
                                planuser.totalprice = price;
                                planuser.monthlyfee = fee;
                                planuser.itemid = item;
                                planuser.statusid = 2;
                                db.tblPlannings.Add(planuser);
                                db.SaveChanges();
                            }
                        }
                        catch (Exception error)
                        {
                            throw error;
                        }
                        BuildEmailTemplate(iduser, idpackag, iditem);
                    }

                    TempData["message"] = "Congratulation! You have Joined Planning Wedding with Our Save Wedding. Please Check Your Email for Your Details Planning!";
                }
                return RedirectToAction("Index");
            }
        }

        public string CheckPriceItem(long[] idi, string idp)
        {
            long price = 0;
            if (idp != "")
            {
                long package = Convert.ToInt64(idp);
                var datadb = db.tblItems.Where(x => x.packageid == package).Select(x => x.itemprice).ToList();
                if (datadb != null)
                {
                    foreach (long d in datadb)
                    {
                        price += d;
                    }
                }
            }
            if (idi != null)
            {
                long[] item = idi;
                foreach (long i in item)
                {
                    var buffer = db.tblItems.Where(x => x.itemid == i).Select(x => x.itemprice).SingleOrDefault();
                    if (buffer != null)
                    {
                        price += Convert.ToInt64(buffer);
                    }
                }
            }
            return price.ToString();
        }

        public JsonResult CountPrice(string item, string package)
        {
            string iditem = item;
            string packages = package;
            List<long> data = new List<long>();
            if (iditem != "")
            {
                string[] buffer = iditem.Split(',');
                foreach (string i in buffer)
                {
                    data.Add(Convert.ToInt64(i));

                }
            }

            if (packages != "")
            {
                int idpackage = Convert.ToInt16(package);
                long[] packagedata = db.tblItems.Where(x => x.packageid == idpackage).Select(x => x.itemid).ToArray();
                if (packagedata != null)
                {
                    foreach (long p in packagedata)
                    {
                        data.Add(p);
                    }
                }
            }

            long pricetotal = 0;
            foreach (long id in data)
            {
                var getdata = db.tblItems.Where(x => x.itemid == id).Select(x => x.itemprice).SingleOrDefault();
                if (getdata != null)
                {
                    pricetotal += Convert.ToInt64(getdata);
                }
            }

            return Json(pricetotal, JsonRequestBehavior.AllowGet);
        }

        public void BuildEmailTemplate(long iduser, int idpackage, long[] iditem)
        {
            string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "PlanningWedding" + ".cshtml");
            var reginfo = db.tblUsers.Where(x => x.userid == iduser).FirstOrDefault();
            var infoplanning = db.tblPlannings.Where(x => x.userid == iduser && x.statusid == 2).Select(x => new { x.plandate, x.longtimesaving, x.totalprice, x.monthlyfee }).FirstOrDefault();
            long price = Convert.ToInt64(infoplanning.totalprice);
            long saving = Convert.ToInt64(infoplanning.monthlyfee);

            var namepackage = db.tblPackages.Where(x => x.packageid == idpackage).FirstOrDefault();
            var packageitems = db.tblItems.Where(x => x.packageid == idpackage).Select(x => new { x.itemname, x.itemprice, x.tblCategory.categoryname }).OrderBy(x => x.categoryname).ToList();
            var bufferprice = db.tblItems.Where(x => x.packageid == idpackage).Select(x => x.itemprice).ToList();
            long packageprice = 0;
            if (bufferprice != null)
            {
                foreach (var buf in bufferprice)
                {
                    packageprice += Convert.ToInt64(buf);
                }
            }
            if (namepackage != null && packageitems != null)
            {
                string itemss = "";
                string distinctcategory = "";
                int nourut = 1;
                foreach (var pi in packageitems)
                {
                    long bufferhargaitem = Convert.ToInt64(pi.itemprice);
                    string buffernamaitem = pi.itemname;
                    string buffercategory = pi.categoryname;
                    if (buffercategory != distinctcategory)
                    {
                        itemss += "<tr><td>" + nourut + ". " + buffercategory + " Category</td><td><strong>:&nbsp;-&nbsp;" + buffernamaitem + "</strong></td><td><strong> Rp." + bufferhargaitem.ToString("N0") + "</strong></td></tr>";
                        nourut++;
                        distinctcategory = buffercategory;
                    }
                    else
                    {
                        itemss += "<tr><td></td><td><strong>&nbsp;&nbsp;-&nbsp;" + buffernamaitem + "</strong></td><td><strong> Rp." + bufferhargaitem.ToString("N0") + "</strong></td></tr>";
                    }
                }
                Body = Body.Replace("@ViewBag.Package", namepackage.packagename);
                Body = Body.Replace("@ViewBag.details", itemss);
                Body = Body.Replace("@ViewBag.hargapaket", "Rp." + packageprice.ToString("N0"));

            }
            else
            {
                Body = Body.Replace("<strong>Package Name  : @ViewBag.Package  @ViewBag.hargapaket</strong><br />", "");
                Body = Body.Replace("<strong>Item @ViewBag.Package : </strong><br /> <table><tbody>@ViewBag.details</tbody></table><br />", "");
            }
            List<itemss> dataitemlist = new List<itemss>();
            if (iditem != null)
            {
                foreach (long id in iditem)
                {
                    var dataitem = db.tblItems.Where(x => x.itemid == id).Select(x => new { x.itemprice, x.itemname, x.tblCategory.categoryname }).FirstOrDefault();
                    if (dataitem != null)
                    {
                        itemss bufferitemsss = new itemss();
                        long pricess = Convert.ToInt64(dataitem.itemprice);
                        string namess = Convert.ToString(dataitem.itemname);
                        string categorynamess = Convert.ToString(dataitem.categoryname);
                        bufferitemsss.itemprice = pricess;
                        bufferitemsss.itemname = namess;
                        bufferitemsss.categoryname = categorynamess;
                        dataitemlist.Add(bufferitemsss);
                    }
                }
            }

            if (dataitemlist != null)
            {
                List<itemss> sortedataitemlist = dataitemlist.OrderBy(x => x.categoryname).ToList();
                string customitem = "";
                int nouruts = 1;
                string distinctcategories = "";
                foreach (itemss di in sortedataitemlist)
                {
                    long bufferharga = Convert.ToInt64(di.itemprice);
                    string buffernama = di.itemname;
                    string buffercategories = di.categoryname;
                    if (buffercategories != distinctcategories)
                    {
                        customitem += "<tr><td>" + nouruts + ". " + buffercategories + " Category</td><td><strong>:&nbsp;-&nbsp;" + buffernama + "</strong></td><td><strong> Rp." + bufferharga.ToString("N0") + "</strong></td></tr>";
                        nouruts++;
                        distinctcategories = buffercategories;
                    }
                    else
                    {
                        customitem += "<tr><td></td><td><strong>&nbsp;&nbsp;-&nbsp;" + buffernama + "</strong></td><td><strong> Rp." + bufferharga.ToString("N0") + "</strong></td></tr>";
                    }
                }
                if (customitem != "")
                {
                    Body = Body.Replace("@ViewBag.Custom", customitem);
                }
                else
                {
                    Body = Body.Replace("<strong>Custom Package Name :</strong><br /> <table><tbody>@ViewBag.Custom</tbody></table><br />", "");
                }
            }
            else
            {
                Body = Body.Replace("@ViewBag.Custom</strong><br />", "");
            }
            Body = Body.Replace("@ViewBag.Name", reginfo.name);
            Body = Body.Replace("@ViewBag.Plandate", infoplanning.plandate);
            Body = Body.Replace("@ViewBag.Month", infoplanning.longtimesaving.ToString());
            Body = Body.Replace("@ViewBag.Price", "Rp." + price.ToString("N0"));
            Body = Body.Replace("@ViewBag.Saving", "Rp." + saving.ToString("N0"));

            Body = Body.ToString();
            BuildEmailTemplate("Congratulation for Joining Our planning Wedding, Hope You Enjoy it.", Body, reginfo.email);
        }

        public static void BuildEmailTemplate(string SubjectText, string BodyText, string SendTo)
        {
            string from, to, bcc, cc, subject, body;
            from = "saveweddingali@gmail.com";
            to = SendTo.Trim();
            bcc = "";
            cc = "";
            subject = SubjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(BodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from, "Save Wedding");
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }

        public static void SendEmail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("saveweddingali@gmail.com", "savewedding123");
            try
            {
                client.Send(mail);
            }
            catch (Exception error)
            {
                throw error;
            }
        }


        public ActionResult EditStatusPlanning(long userid, int oldstatus, string dateplan)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var dataplanning = db.tblPlannings.Where(x => x.userid == userid && x.statusid == oldstatus && x.plandate == dateplan).ToList();
                if (dataplanning != null)
                {
                    string id = Request.Params["statusid"];
                    if (id != "")
                    {
                        int idstatus = Convert.ToInt32(id);
                        foreach (var item in dataplanning)
                        {
                            try
                            {
                                item.statusid = idstatus;
                                db.SaveChanges();
                                TempData["messagealert"] = "Success to change Status Planning";
                            }
                            catch (Exception error)
                            {
                                throw error;
                            }
                        }
                    }
                }
                return RedirectToAction("Planning", "Admin");
            }
        }

        public ActionResult DeletePlanning(long iduser, int idstatus, string dateplan)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                try
                {
                    var dataplanning = db.tblPlannings.Where(x => x.userid == iduser && x.statusid == idstatus && x.plandate == dateplan).ToList();
                    if (dataplanning != null)
                    {
                        //delete all data in database
                        foreach (var item in dataplanning)
                        {
                            db.tblPlannings.Remove(item);
                            db.SaveChanges();
                        }
                        TempData["messagealert"] = "Customer's Planning is Successed to be Deleted";
                    }
                    else
                    {
                        TempData["messagealert"] = "Item is not Found!";
                    }
                }
                catch (Exception error)
                {
                    throw error;
                }
                return RedirectToAction("Planning", "Admin");
            }
        }

    }
}