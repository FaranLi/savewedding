﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class UserController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: User
        public ActionResult Index()
        {
            //ViewBag.Fullname = logedUserFullname;

            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblItem> itemdata = db.tblItems.ToList();
                List<tblReward> rewarddata = db.tblRewards.ToList();
                RewardItem datalist = new RewardItem();
                datalist.Reward = rewarddata;
                datalist.Item = itemdata;
                return View(datalist);
            }
        }

        public ActionResult ViewProfile()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                int id = Convert.ToInt32(Session["logedUserId"]);
                var userprofile = db.tblUsers.Where(x => x.userid == id).FirstOrDefault();
                return View(userprofile);
            }

        }
        [HttpPost]
        public ActionResult ViewProfile(tblUser user, HttpPostedFileBase image1)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var userdata = db.tblUsers.Where(x => x.userid == user.userid).SingleOrDefault();
                //Update fields
                if (userdata != null)
                {
                    try
                    {
                        if (image1 != null)
                        {
                            //delete old image before update
                            string fullPath = Request.MapPath(userdata.imgprofile);
                            if (System.IO.File.Exists(fullPath) && userdata.imgprofile != "~/Templates/Images/User/default.png")
                            {
                                System.IO.File.Delete(fullPath);
                            }
                            string fileName = Path.GetFileNameWithoutExtension(image1.FileName);
                            string extension = Path.GetExtension(image1.FileName);
                            fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                            userdata.imgprofile = "~/Templates/Images/User/" + fileName;
                            fileName = Path.Combine(Server.MapPath("~/Templates/Images/User/"), fileName);
                            image1.SaveAs(fileName);
                        }
                        userdata.name = user.name;
                        userdata.username = user.username;
                        userdata.phone = user.phone;
                        userdata.noktp = user.noktp;
                        userdata.Gender = user.Gender;
                        userdata.address = user.address;


                        db.SaveChanges();
                        Session["logedUserImage"] = userdata.imgprofile;
                        Session["logedUsername"] = userdata.username;
                        return RedirectToAction("ViewProfile", "User");
                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("ViewProfile", "User");
            }
        }
        public ActionResult ChangePassword()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                return View();
            }

        }
        [HttpPost]
        public ActionResult ChangePassword(string Kode, tblUser item)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                try
                {
                    var emailuser = Session["logedEmail"].ToString();
                    var cp = db.tblUsers.Where(a => a.email.Equals(emailuser)).SingleOrDefault();
                    if (cp != null)
                    {

                        if (string.Compare(Crypto.Hash(item.password), cp.password) == 0)
                        {

                            cp.password = Crypto.Hash(Kode);
                            db.SaveChanges();
                            TempData["success"] = "Your password has been changed!";
                            return RedirectToAction("ChangePassword");
                        }
                        else
                        {
                            TempData["error"] = "Your old password is wrong !";
                            return View();
                        }
                    }
                    else
                    {
                        ViewBag.DuplicateMessage = "Invalid email";

                    }
                    return View("ChangePassword");
                }
                catch (Exception error)
                {
                    TempData["error"] = error.Message;
                    return View();
                }
            }
        }

    }
}