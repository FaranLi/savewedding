﻿using SaveWedding.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class SavingController : Controller
    {
        DBEntities db = new DBEntities();
        // GET: Saving
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                long iduser = Convert.ToInt64(Session["logedUserId"]);
                List<tblSaving> savingdata = db.tblSavings.Where(x => x.userid == iduser).ToList();

                return View(savingdata);
            }


        }
        [HttpPost]
        public ActionResult Index(string nominal, HttpPostedFileBase imagesaving)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                long iduser = Convert.ToInt64(Session["logedUserId"]);
                var validasi = db.tblSavings.Where(x => x.userid == iduser).FirstOrDefault();
                var data = db.tblPlannings.Where(x => x.userid == iduser && x.statusid == 2).Select(x => new { x.longtimesaving, x.monthlyfee, x.statusid, x.totalprice }).FirstOrDefault();
                tblSaving save = new tblSaving();
                if (validasi == null)
                {
                    try
                    {
                        if (data != null)
                        {
                            long sisa = Convert.ToInt64( data.totalprice - data.monthlyfee);

                            if (imagesaving != null)
                            {
                                string fileName = Path.GetFileNameWithoutExtension(imagesaving.FileName);
                                string extension = Path.GetExtension(imagesaving.FileName);
                                fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                                save.savingimage = "~/Templates/Images/Saving/" + fileName;
                                fileName = Path.Combine(Server.MapPath("~/Templates/Images/Saving/"), fileName);
                                imagesaving.SaveAs(fileName);
                            }
                            save.uploaddate = DateTime.Now;
                            save.savingdate = DateTime.Now.AddMonths(1);
                            if (DateTime.Today.Month == 2)
                            {
                                if ((DateTime.Today.Year % 4) == 0)
                                {
                                    save.deadlinedate = DateTime.Now.AddDays(36);
                                }
                                else
                                {
                                    save.deadlinedate = DateTime.Now.AddDays(35);
                                }
                            }
                            else if ((DateTime.Today.Month % 2) == 0)
                            {
                                save.deadlinedate = DateTime.Now.AddDays(37);
                            }
                            else
                            {
                                save.deadlinedate = DateTime.Now.AddDays(38);
                            }
                            save.userid = iduser;
                            save.nominal = data.monthlyfee;
                            save.remaining = sisa;
                            save.timerange = data.longtimesaving;
                            db.tblSavings.Add(save);
                            db.SaveChanges();
                            double point;
                            int jum;
                            var savingdata = db.tblSavings.Where(x => x.userid == iduser).FirstOrDefault();
                            if (savingdata.point == null)
                            {
                                point = 0 + (0.1 * Convert.ToDouble(data.monthlyfee));
                                save.point = Math.Round(point, 0);
                                db.Entry(save).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                            if (savingdata.monthto == null)
                            {
                                jum = 1;
                                save.monthto = jum;
                                db.Entry(save).State = EntityState.Modified;
                                db.SaveChanges();
                            }

                            TempData["successsave"] = "Your saving is success, teams will check immediately";

                        }
                        else
                        {
                            TempData["Error"] = "Please input Planning Wedding First";
                        }

                    }
                    catch (Exception error)
                    {
                        throw error;

                    }
                }
                else
                {
                    var savingdata = db.tblSavings.Where(x => x.userid == iduser).OrderByDescending(x => x.savingid).FirstOrDefault();
                    try
                    {
                        if (savingdata.monthto != data.longtimesaving)
                        {
                            if (data != null)
                            {
                                if (savingdata.uploaddate.Value.Month != DateTime.Today.Month)
                                {

                                    if (DateTime.Today > savingdata.deadlinedate)
                                    {
                                        double denda = 0.05 * Convert.ToDouble(data.monthlyfee);
                                        long nomfine = Convert.ToInt64(Math.Round(denda, 0) + data.monthlyfee);
                                        long remain = Convert.ToInt64( savingdata.remaining - data.monthlyfee);

                                        string fileName = Path.GetFileNameWithoutExtension(imagesaving.FileName);
                                        string extension = Path.GetExtension(imagesaving.FileName);
                                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;
                                        save.uploaddate = DateTime.Now;
                                        save.savingdate = savingdata.savingdate.Value.AddMonths(1);
                                        if (DateTime.Today.Month == 2)
                                        {
                                            if ((DateTime.Today.Year % 4) == 0)
                                            {
                                                save.deadlinedate = savingdata.deadlinedate.Value.AddDays(36);
                                            }
                                            else
                                            {
                                                save.deadlinedate = savingdata.deadlinedate.Value.AddDays(35);
                                            }
                                        }
                                        else if ((DateTime.Today.Month % 2) == 0)
                                        {
                                            save.deadlinedate = savingdata.deadlinedate.Value.AddDays(37);
                                        }
                                        else
                                        {
                                            save.deadlinedate = savingdata.deadlinedate.Value.AddDays(38);
                                        }
                                        save.userid = iduser;
                                        save.nominal = data.monthlyfee;
                                        save.fine = denda;
                                        save.remaining = remain;
                                        save.timerange = data.longtimesaving;
                                        save.savingimage = "~/Templates/Images/Saving/" + fileName;
                                        fileName = Path.Combine(Server.MapPath("~/Templates/Images/Saving/"), fileName);
                                        imagesaving.SaveAs(fileName);
                                        db.tblSavings.Add(save);
                                        db.SaveChanges();
                                        double point;
                                        int jum;

                                        point = Convert.ToDouble(savingdata.point);
                                        save.point = Math.Round(point, 0);
                                        if (savingdata.monthto != null)
                                        {
                                            jum = Convert.ToInt32(savingdata.monthto) + 1;
                                            save.monthto = jum;
                                        }
                                        db.Entry(save).State = EntityState.Modified;
                                        db.SaveChanges();

                                        TempData["successsave"] = "Your saving is success, teams will check immediately";
                                        //ViewBag.Successsave = "Your saving is success, teams will check immediately";

                                    }
                                    else
                                    {
                                        long remain = Convert.ToInt64(savingdata.remaining - data.monthlyfee);

                                        string fileName = Path.GetFileNameWithoutExtension(imagesaving.FileName);
                                        string extension = Path.GetExtension(imagesaving.FileName);
                                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                                        save.uploaddate = DateTime.Now;
                                        save.savingdate = savingdata.savingdate.Value.AddMonths(1);
                                        if (DateTime.Today.Month == 2)
                                        {
                                            if ((DateTime.Today.Year % 4) == 0)
                                            {
                                                save.deadlinedate = savingdata.deadlinedate.Value.AddDays(36);
                                            }
                                            else
                                            {
                                                save.deadlinedate = savingdata.deadlinedate.Value.AddDays(35);
                                            }
                                        }
                                        else if ((DateTime.Today.Month % 2) == 0)
                                        {
                                            save.deadlinedate = savingdata.deadlinedate.Value.AddDays(37);
                                        }
                                        else
                                        {
                                            save.deadlinedate = savingdata.deadlinedate.Value.AddDays(38);
                                        }
                                        save.userid = iduser;
                                        save.nominal = data.monthlyfee;
                                        save.remaining = remain;
                                        save.timerange = data.longtimesaving;
                                        save.savingimage = "~/Templates/Images/Saving/" + fileName;
                                        fileName = Path.Combine(Server.MapPath("~/Templates/Images/Saving/"), fileName);
                                        imagesaving.SaveAs(fileName);
                                        db.tblSavings.Add(save);
                                        db.SaveChanges();
                                        double point;
                                        int jum;

                                        if (savingdata.point == 0)
                                        {
                                            point = 0 + (0.1 * Convert.ToDouble(data.monthlyfee));
                                            save.point = Math.Round(point, 0);
                                        }
                                        else
                                        {
                                            point = Convert.ToDouble(savingdata.point) + (0.1 * Convert.ToDouble(data.monthlyfee));
                                            save.point = Math.Round(point, 0);
                                        }

                                        if (savingdata.monthto != null)
                                        {
                                            jum = Convert.ToInt32(savingdata.monthto) + 1;
                                            save.monthto = jum;
                                        }
                                        db.Entry(save).State = EntityState.Modified;
                                        db.SaveChanges();


                                        TempData["successsave"] = "Your saving is success, teams will check immediately";

                                    }

                                }

                                else
                                {
                                    TempData["Error"] = "This month you have saved money";
                                }
                            }
                            else
                            {
                                TempData["Error"] = "Please input Planning Wedding First";
                            }
                        }
                        else
                        {

                            TempData["finish"] = "Your Save Wedding Finished, please contact us in ";
                        }
                    }
                    catch (Exception error)
                    {
                        throw error;

                    }
                }

                return RedirectToAction("Index", "Saving");
            }

        }
        public ActionResult EditSaving(HttpPostedFileBase imagesaving)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                long iduser = Convert.ToInt64(Session["logedUserId"]);
                var datasave = db.tblSavings.Where(x => x.userid == iduser).OrderByDescending(x => x.savingid).FirstOrDefault();
                try
                {
                    if (imagesaving != null)
                    {
                        //delete old image before update
                        string fullPath = Request.MapPath(datasave.savingimage);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }
                        string fileName = Path.GetFileNameWithoutExtension(imagesaving.FileName);
                        string extension = Path.GetExtension(imagesaving.FileName);
                        fileName = fileName + DateTime.Now.ToString("yymmssfff") + extension;

                        datasave.savingimage = "~/Templates/Images/Saving/" + fileName;
                        fileName = Path.Combine(Server.MapPath("~/Templates/Images/Saving/"), fileName);
                        imagesaving.SaveAs(fileName);
                    }
                    datasave.uploaddate = DateTime.Now;
                    datasave.savingstatus = null;
                    db.SaveChanges();
                    TempData["successsave"] = "Your saving is success, teams will check immediately";
                }
                catch (Exception error)
                {
                    throw error;
                }
                return RedirectToAction("Index", "Saving");
            }
        }


        public ActionResult EditStatusSaving(long saveid, int status)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                var savingdata = db.tblSavings.Where(x => x.savingid == saveid).SingleOrDefault();
                long idsaving = Convert.ToInt64(savingdata.savingid);
                long iduser = Convert.ToInt64(savingdata.userid);
                var data = db.tblPlannings.Where(x => x.userid == iduser && x.statusid == 2).Select(x => new { x.longtimesaving, x.monthlyfee, x.statusid }).FirstOrDefault();


                if (savingdata != null)
                {
                    try
                    {
                        if (status == 1 && savingdata.savingstatus != 1)
                        {
                            savingdata.savingstatus = 1;
                            db.SaveChanges();
                            int stat = Convert.ToInt32(savingdata.savingstatus);
                            BuildEmailTemplate(idsaving, iduser, stat);
                            if (savingdata.monthto == data.longtimesaving)
                            {
                                var dataplanning = db.tblPlannings.Where(x => x.userid == iduser).ToList();
                                if (dataplanning != null)
                                {
                                    int planstat = 1;
                                    foreach (var item in dataplanning)
                                    {
                                        try
                                        {
                                            item.statusid = planstat;
                                            db.SaveChanges();
                                        }
                                        catch (Exception error)
                                        {
                                            throw error;
                                        }
                                    }
                                }

                                BuildEmailTemplate(idsaving, iduser, stat);
                            }
                            TempData["messagealert"] = "Success to change Status Saving";
                        }
                        else if (status == 2 && savingdata.savingstatus != 2)
                        {
                            savingdata.savingstatus = 2;
                            db.SaveChanges();
                            int stat = Convert.ToInt32(savingdata.savingstatus);
                            BuildEmailTemplate(idsaving, iduser, stat);
                            TempData["messagealert"] = "Success to change Status Saving";
                        }
                        else
                        {
                            TempData["messagealert1"] = "Failed to change, status was same before.";
                        }

                    }
                    catch (Exception error)
                    {
                        throw error;
                    }
                }
                return RedirectToAction("Saving", "Admin");
            }

        }
        public void BuildEmailTemplate(long idsaving, long iduser, int stat)
        {
            if (stat == 1)
            {
                string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "ApproveSaving" + ".cshtml");
                var reginfo = db.tblUsers.Where(x => x.userid == iduser).FirstOrDefault();
                var infosaving = db.tblSavings.Where(x => x.savingid == idsaving).Select(x => new { x.uploaddate, x.savingdate, x.nominal, x.deadlinedate, x.point }).FirstOrDefault();
                var infoplan = db.tblPlannings.Where(x => x.userid == iduser).FirstOrDefault();
                long total = Convert.ToInt64(infosaving.nominal);
                double point = Math.Round((Convert.ToDouble(infosaving.point)), 0);

                Body = Body.Replace("@ViewBag.Name", reginfo.name);
                Body = Body.Replace("@ViewBag.Upload", infosaving.uploaddate.Value.ToString("dd/MM/yyyy HH:mm"));
                Body = Body.Replace("@ViewBag.Month", infosaving.savingdate.Value.ToString("dd/MM/yyyy"));
                Body = Body.Replace("@ViewBag.Deadline", infosaving.deadlinedate.Value.ToString("dd/MM/yyyy HH:mm"));
                Body = Body.Replace("@ViewBag.Price", total.ToString("C0", CultureInfo.CreateSpecificCulture("id-ID")));
                Body = Body.Replace("@ViewBag.Point", point.ToString("N0"));
                Body = Body.ToString();
                BuildEmailTemplate("Saving Accepted ", Body, reginfo.email);
                if (infoplan.statusid == 1)
                {
                    string Body1 = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "FinishPlanning" + ".cshtml");
                    var reginfo1 = db.tblUsers.Where(x => x.userid == iduser).FirstOrDefault();

                    Body1 = Body1.Replace("@ViewBag.Name", reginfo1.name);
                    Body1 = Body1.ToString();
                    BuildEmailTemplate("Save Wedding is Done ! ", Body1, reginfo1.email);
                }
            }
            else
            {
                string Body = System.IO.File.ReadAllText(HostingEnvironment.MapPath("~/Templates/EmailTemp/") + "RejectSaving" + ".cshtml");
                var reginfo = db.tblUsers.Where(x => x.userid == iduser).FirstOrDefault();
                var infosaving = db.tblSavings.Where(x => x.savingid == idsaving).Select(x => new { x.uploaddate, x.savingdate, x.nominal, x.deadlinedate, x.point }).FirstOrDefault();

                long total = Convert.ToInt64(infosaving.nominal);
                Body = Body.Replace("@ViewBag.Name", reginfo.name);
                Body = Body.Replace("@ViewBag.Price", total.ToString("C0", CultureInfo.CreateSpecificCulture("id-ID")));
                Body = Body.ToString();
                BuildEmailTemplate("Saving Rejected ", Body, reginfo.email);
            }

        }

        public static void BuildEmailTemplate(string SubjectText, string BodyText, string SendTo)
        {
            string from, to, bcc, cc, subject, body;
            from = "saveweddingali@gmail.com";
            to = SendTo.Trim();
            bcc = "";
            cc = "";
            subject = SubjectText;
            StringBuilder sb = new StringBuilder();
            sb.Append(BodyText);
            body = sb.ToString();
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(from, "Save Wedding");
            mail.To.Add(new MailAddress(to));
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.Bcc.Add(new MailAddress(bcc));
            }
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(new MailAddress(cc));
            }
            mail.Subject = subject;
            mail.Body = body;
            mail.IsBodyHtml = true;
            SendEmail(mail);
        }

        public static void SendEmail(MailMessage mail)
        {
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            client.UseDefaultCredentials = false;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new System.Net.NetworkCredential("saveweddingali@gmail.com", "savewedding123");
            try
            {
                client.Send(mail);
            }
            catch (Exception error)
            {
                throw error;
            }
        }
    }
}