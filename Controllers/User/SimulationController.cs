﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class SimulationController : Controller
    {
        // GET: Simulation
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblSalary> salarydata = db.tblSalaries.ToList();
                List<tblCity> citydata = db.tblCities.ToList();
                List<tblJob> jobdata = db.tblJobs.ToList();
                List<tblUser> userdata = db.tblUsers.ToList();


                JobSalaryCityUser datalist = new JobSalaryCityUser();
                datalist.Salary = salarydata;
                datalist.City = citydata;
                datalist.Job = jobdata;
                datalist.User = userdata;

                return View(datalist);
            }


        }

        public JsonResult CountSimulation(string city, string salary, string proffesion)
        {
            var data = db.tblPackages.Select(x => x.packagename).ToArray();
            string[] saran = new string[4];
            string finalresult = "";
            string paramage = Request.Params["age"];
            if (data != null)
            {
                int age = Convert.ToInt32(paramage);
                //validasi salary
                string[] packagelist = data;
                if (salary == "7")
                {
                    saran[0] = packagelist[0];
                }
                else if (salary == "8")
                {
                    saran[0] = packagelist[1];
                }
                else
                {
                    saran[0] = packagelist[2];
                }

                //validasi profesi
                if (proffesion == "1")
                {
                    saran[1] = packagelist[0];
                }
                else if (proffesion == "3" || proffesion == "4")
                {
                    saran[1] = packagelist[1];
                }
                else
                {
                    saran[1] = packagelist[2];
                }

                //validasi city
                if (city == "1159" || city == "1162")
                {
                    saran[2] = packagelist[2];
                }
                else if (city == "1160" || city == "1161" || city == "1163")
                {
                    saran[2] = packagelist[1];
                }
                else
                {
                    saran[2] = packagelist[0];
                }

                //validasi age
                if (age >= 18 && age <= 25)
                {
                    saran[3] = packagelist[0];
                }
                else if (age >= 26 && age <= 33)
                {
                    saran[3] = packagelist[1];
                }
                else
                {
                    saran[3] = packagelist[2];
                }

                if (saran != null)
                {
                    int paket1 = 0;
                    int paket2 = 0;
                    string hasil = saran[0];
                    string hasil2 = saran[1];

                    foreach (string item in saran)
                    {
                        if (item == hasil)
                        {
                            paket1 += 1;
                            if (paket1 >= 2)
                            {
                                finalresult = item;
                                break;
                            }
                        }
                        else if (item == hasil2)
                        {
                            paket2 += 1;
                            if (paket2 >= 2)
                            {
                                finalresult = item;
                                break;
                            }
                        }
                        else
                        {
                            finalresult = packagelist[1];


                        }
                    }
                }
            }


            return Json(finalresult, JsonRequestBehavior.AllowGet);
        }
    }
}