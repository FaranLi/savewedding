﻿using SaveWedding.Models;
using SaveWedding.Models.Admin;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SaveWedding.Controllers.User
{
    public class InformationController : Controller
    {
        // GET: Information
        DBEntities db = new DBEntities();
        public ActionResult Index()
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "2")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                List<tblPackage> packagedata = db.tblPackages.ToList();
                List<tblItem> itemdata = db.tblItems.ToList();
                List<tblCategory> categorydata = db.tblCategories.ToList();

                PackageItemCategory datalist = new PackageItemCategory();
                datalist.Package = packagedata;
                datalist.Item = itemdata;
                datalist.Category = categorydata;

                return View(datalist);
            }

        }
    }
}