﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class GetPointReportingController : Controller
    {
        DBEntities db = new DBEntities();
        PointDataSet ds = new PointDataSet();
        ReportViewer report = new ReportViewer();
        // GET: GetPoint      
        public ActionResult Points(string filter, string idmonth)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        if (idmonth != null || idmonth != "")
                        {
                            int idmon = Convert.ToInt32(idmonth);
                            queries = "select * from GetPointData where savingstatus IS NULL AND MONTH(uploaddate) =" + idmon;
                        }
                        break;
                    default:
                        queries = "select * from GetPointData";
                        break;
                }
                GetDataPoint(queries);
                var monthdata = db.tblMonths.ToList();
                return View(monthdata);
            }
        }

        public void GetDataPoint(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.GetPointData.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\PointReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetPoint", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.PointReportViewer = report;
        }
    }
}