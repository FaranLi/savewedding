﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class ClaimRewardReportingController : Controller
    {
        DBEntities db = new DBEntities();
        ClaimRewardDataSet ds = new ClaimRewardDataSet();
        ReportViewer report = new ReportViewer();
        // GET: ClaimReward
        public ActionResult Claims(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        queries = "select * from ClaimRewardData where claimstatus =" + 1;
                        break;
                    case "2":
                        queries = "select * from ClaimRewardData where claimstatus =" + 0;
                        break;
                    default:
                        queries = "select * from ClaimRewardData";
                        break;
                }
                GetDataClaim(queries);

                return View();
            }
        }

        public void GetDataClaim(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.ClaimRewardData.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\ClaimRewardReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetClaim", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.ClaimReportViewer = report;
        }
    }
}