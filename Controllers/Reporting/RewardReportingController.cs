﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class RewardReportingController : Controller
    {
        DBEntities db = new DBEntities();
        RewardDataSet ds = new RewardDataSet();
        ReportViewer report = new ReportViewer();
        // GET: RewardReporting    
        public ActionResult Rewards(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        queries = "select * from tblReward where status =" + 1;
                        break;
                    case "2":
                        queries = "select * from tblReward where status =" + 0;
                        break;
                    default:
                        queries = "select * from tblReward";
                        break;
                }
                GetDataReward(queries);
                return View();
            }
        }

        public void GetDataReward(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.tblReward.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\RewardReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetReward", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.RewardReportViewer = report;
        }
    }
}