﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class PlanningReportingController : Controller
    {
        PlanningDataSet ds = new PlanningDataSet();
        ReportViewer report = new ReportViewer();
        // GET: PlanningReporting  
        public ActionResult Plannings(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        queries = "select * from PlanningData where statusid =" + 2;
                        break;
                    case "2":
                        queries = "select * from PlanningData where statusid =" + 1;
                        break;
                    default:
                        queries = "select * from PlanningData";
                        break;
                }
                GetDataPlanning(queries);
                return View();
            }
        }

        public void GetDataPlanning(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.PlanningData.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\PlanningReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetPlanning", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.PlanningReportViewer = report;
        }
    }
}