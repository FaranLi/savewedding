﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class SavingReportingController : Controller
    {
        DBEntities db = new DBEntities();
        SavingDataSet ds = new SavingDataSet();
        ReportViewer report = new ReportViewer();
        // GET: SavingReporting
        public ActionResult Savings(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        queries = "select * from SavingData where fine IS NULL";
                        break;
                    case "2":
                        queries = "select * from SavingData where fine IS NOT NULL";
                        break;
                    default:
                        queries = "select * from SavingData";
                        break;
                }
                GetDataSaving(queries);
                var datasaving = db.tblSavings.ToList();
                return View(datasaving);
            }
        }

        public void GetDataSaving(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.SavingData.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\SavingReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetSaving", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.SavingReportViewer = report;
        }
    }
}

