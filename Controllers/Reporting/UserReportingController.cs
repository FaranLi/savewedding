﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class UserReportingController : Controller
    {
        DBEntities db = new DBEntities();
        UserDataSet ds = new UserDataSet();
        ReportViewer report = new ReportViewer();
        // GET: UserReporting
        public ActionResult Users(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";

                switch (filter)
                {
                    case "1":
                        queries = "select * from tblUser where actorid =" + 1;
                        break;
                    case "2":
                        queries = "select * from tblUser where actorid =" + 2;
                        break;
                    case "3":
                        queries = "select * from tblUser where isactive =" + 1;
                        break;
                    case "4":
                        queries = "select * from tblUser where isactive =" + 0;
                        break;
                    default:
                        queries = "select * from tblUser";
                        break;
                }
                GetDataUser(queries);
                var dataactor = db.tblActors.ToList();
                return View(dataactor);
            }
        }

        public void GetDataUser(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.tblUser.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\UserReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetUser", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.UserReportViewer = report;
        }
    }
}