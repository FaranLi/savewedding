﻿using Microsoft.Reporting.WebForms;
using SaveWedding.Datasets;
using SaveWedding.Models;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace SaveWedding.Controllers.Reporting
{
    public class ItemReportingController : Controller
    {
        DBEntities db = new DBEntities();
        ItemDataSet ds = new ItemDataSet();
        ReportViewer report = new ReportViewer();
        // GET: Item    
        public ActionResult Items(string filter)
        {
            string actorids = Session["logedActorId"].ToString();
            if (Session["logedEmail"] == null || Session["logedUserImage"] == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (actorids != "1")
            {
                return RedirectToAction("PermissionAccess", "Home");
            }
            else
            {
                report.ProcessingMode = ProcessingMode.Local;
                report.Width = Unit.Pixel(930);
                string queries = "";
                switch (filter)
                {
                    case "1":
                        queries = "select * from ItemData where categoryid =" + 1;
                        break;
                    case "2":
                        queries = "select * from ItemData where categoryid =" + 2;
                        break;
                    case "3":
                        queries = "select * from ItemData where categoryid =" + 3;
                        break;
                    case "4":
                        queries = "select * from ItemData where categoryid =" + 4;
                        break;
                    default:
                        queries = "select * from ItemData";
                        break;
                }
                GetDataItem(queries);
                var datacategory = db.tblCategories.ToList();
                return View(datacategory);
            }
        }

        public void GetDataItem(string queries)
        {
            //Connect to Database
            var connectionString = ConfigurationManager.ConnectionStrings["DbSaveWeddingConnectionString"].ConnectionString;
            SqlConnection conn = new SqlConnection(connectionString);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(queries, conn);
            dataAdapter.Fill(ds, ds.ItemData.TableName);
            //get data report and send to rdlc
            report.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"Report\ItemReport.rdlc";
            report.LocalReport.DataSources.Clear();
            report.LocalReport.DataSources.Add(new ReportDataSource("DataSetItem", ds.Tables[0]));
            report.LocalReport.Refresh();
            //save data to viewbag
            ViewBag.ItemReportViewer = report;
        }
    }
}



